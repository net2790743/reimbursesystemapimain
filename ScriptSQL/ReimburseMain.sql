USE [master]
GO
/****** Object:  Database [ReimburseMain]    Script Date: 29/10/2023 15:17:06 ******/
CREATE DATABASE [ReimburseMain]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ReimburseMain', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\ReimburseMain.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ReimburseMain_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\ReimburseMain_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ReimburseMain] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ReimburseMain].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ReimburseMain] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ReimburseMain] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ReimburseMain] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ReimburseMain] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ReimburseMain] SET ARITHABORT OFF 
GO
ALTER DATABASE [ReimburseMain] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ReimburseMain] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ReimburseMain] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ReimburseMain] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ReimburseMain] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ReimburseMain] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ReimburseMain] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ReimburseMain] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ReimburseMain] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ReimburseMain] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ReimburseMain] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ReimburseMain] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ReimburseMain] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ReimburseMain] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ReimburseMain] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ReimburseMain] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ReimburseMain] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ReimburseMain] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ReimburseMain] SET  MULTI_USER 
GO
ALTER DATABASE [ReimburseMain] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ReimburseMain] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ReimburseMain] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ReimburseMain] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ReimburseMain] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ReimburseMain] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [ReimburseMain] SET QUERY_STORE = OFF
GO
USE [ReimburseMain]
GO
/****** Object:  User [superadmin]    Script Date: 29/10/2023 15:17:06 ******/
CREATE USER [superadmin] FOR LOGIN [superadmin] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Reimburse]    Script Date: 29/10/2023 15:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reimburse](
	[ID] [uniqueidentifier] NOT NULL,
	[ReimburseNo] [nvarchar](50) NOT NULL,
	[EmployeeId] [uniqueidentifier] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Reimburse] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReimburseBudget]    Script Date: 29/10/2023 15:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReimburseBudget](
	[ID] [uniqueidentifier] NOT NULL,
	[ReimburseTypeId] [uniqueidentifier] NOT NULL,
	[JobClass] [nvarchar](20) NOT NULL,
	[Budget] [decimal](18, 2) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ReimburseBudget] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReimburseDetail]    Script Date: 29/10/2023 15:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReimburseDetail](
	[ID] [uniqueidentifier] NOT NULL,
	[ReimburseId] [uniqueidentifier] NOT NULL,
	[TotalClaim] [decimal](18, 2) NOT NULL,
	[ReimburseTypeId] [uniqueidentifier] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ReimburseDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReimburseType]    Script Date: 29/10/2023 15:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReimburseType](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[ResetDuration] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ReimburseType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Reimburse] ([ID], [ReimburseNo], [EmployeeId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'6b42b9be-08b9-4b07-9695-0fcb57f5ee25', N'A788SJSS', N'187ae7ef-09d4-48e0-a8ed-0c25c7245a37', 0, CAST(N'2023-10-29T12:07:06.823' AS DateTime), N'wl', CAST(N'2023-10-29T12:07:06.823' AS DateTime), N'wl')
GO
INSERT [dbo].[ReimburseBudget] ([ID], [ReimburseTypeId], [JobClass], [Budget], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'9b5fe9e9-605a-4d9f-92a8-ad8ef818b4f7', N'7d9717bf-b4c9-4541-b701-f07736f14cb6', N'D', CAST(1500000.00 AS Decimal(18, 2)), 0, CAST(N'2023-10-29T12:46:30.390' AS DateTime), N'wl', CAST(N'2023-10-29T12:46:30.390' AS DateTime), N'wl')
GO
INSERT [dbo].[ReimburseDetail] ([ID], [ReimburseId], [TotalClaim], [ReimburseTypeId], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'e2586c0b-236d-41fe-851e-0c60081c2bc2', N'6b42b9be-08b9-4b07-9695-0fcb57f5ee25', CAST(1000000.00 AS Decimal(18, 2)), N'7d9717bf-b4c9-4541-b701-f07736f14cb6', 0, CAST(N'2023-10-29T12:49:09.833' AS DateTime), N'wl', CAST(N'2023-10-29T12:49:09.833' AS DateTime), N'wl')
GO
INSERT [dbo].[ReimburseType] ([ID], [Name], [ResetDuration], [IsDeleted], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'7d9717bf-b4c9-4541-b701-f07736f14cb6', N'Kaca Mata', 12, 0, CAST(N'2023-10-29T12:43:32.690' AS DateTime), N'wl', CAST(N'2023-10-29T12:43:32.690' AS DateTime), N'wl')
GO
ALTER TABLE [dbo].[ReimburseBudget]  WITH CHECK ADD  CONSTRAINT [FK_ReimburseBudget_ReimburseType] FOREIGN KEY([ReimburseTypeId])
REFERENCES [dbo].[ReimburseType] ([ID])
GO
ALTER TABLE [dbo].[ReimburseBudget] CHECK CONSTRAINT [FK_ReimburseBudget_ReimburseType]
GO
ALTER TABLE [dbo].[ReimburseDetail]  WITH CHECK ADD  CONSTRAINT [FK_ReimburseDetail_Reimburse] FOREIGN KEY([ReimburseId])
REFERENCES [dbo].[Reimburse] ([ID])
GO
ALTER TABLE [dbo].[ReimburseDetail] CHECK CONSTRAINT [FK_ReimburseDetail_Reimburse]
GO
ALTER TABLE [dbo].[ReimburseDetail]  WITH CHECK ADD  CONSTRAINT [FK_ReimburseDetail_ReimburseType] FOREIGN KEY([ReimburseTypeId])
REFERENCES [dbo].[ReimburseType] ([ID])
GO
ALTER TABLE [dbo].[ReimburseDetail] CHECK CONSTRAINT [FK_ReimburseDetail_ReimburseType]
GO
USE [master]
GO
ALTER DATABASE [ReimburseMain] SET  READ_WRITE 
GO

﻿using System;
using ReimburseApi.Repositories.Context;
using ReimburseApi.EFReimburseEntities;
using ReimburseApi.Helpers;
using Microsoft.Extensions.Options;
using Dapper;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace ReimburseApi.Repositories
{
    public interface IReimburseRepository
    {
        public List<Reimburse> GetById(Guid reimId);
        public List<Reimburse> GetAll();
        public ReimburseDetail GetByReimburseDetail(Guid reimId);
        public Reimburse GetByReimburseId(Guid reimId);
        public ReimburseType GetReimburseType(Guid typeId);
        public void Update(EFReimburseEntities.Reimburse emp);
        public void Insert(EFReimburseEntities.Reimburse emp, string createdBy);

    }
    public class ReimburseRepository : IReimburseRepository
    {
        private readonly DapperContext _context;
        private readonly IOptions<AppSettings> _config;

        public ReimburseRepository(DapperContext context, IOptions<AppSettings> config)
        {
            _context = context;
            _config = config;
        }

        public List<Reimburse> GetById(Guid reimId)
        {
            string sql = "SELECT TOP 1 * FROM Reimburse with(nolock) WHERE EmployeeId = @reimburseID and IsDeleted = 0";

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var reimburse = connection.Query<Reimburse>(sql, new { reimburseID = reimId });
                    return reimburse.ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        public List<Reimburse> GetAll()
        {
            string sql = "SELECT * FROM Reimburse with(nolock)";

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var reimburse = connection.Query<Reimburse>(sql);
                    return reimburse.ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        public ReimburseDetail GetByReimburseDetail(Guid reimId)
        {
            string sql = "SELECT TOP 1 * FROM ReimburseDetail with(nolock) WHERE ReimburseId = @reimburseID and IsDeleted = 0";

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var reimburse = connection.Query<ReimburseDetail>(sql, new { reimburseID = reimId });
                    return reimburse.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        public Reimburse GetByReimburseId(Guid reimId)
        {
            string sql = "SELECT TOP 1 * FROM Reimburse with(nolock) WHERE Id = @reimburseID and IsDeleted = 0";

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var reimburse = connection.Query<Reimburse>(sql, new { reimburseID = reimId });
                    return reimburse.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }

        }

        public ReimburseType GetReimburseType(Guid typeId)
        {
            string sql = "SELECT TOP 1 * FROM ReimburseType with(nolock) WHERE Id = @reimburseTypeID and IsDeleted = 0";

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var reimburse = connection.Query<ReimburseType>(sql, new { reimburseTypeID = typeId });
                    return reimburse.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }

        }

        public void Update(EFReimburseEntities.Reimburse emp)
        {
            string sql = "update Employee set Nik = @Nik, Role = @Role WHERE Id = @EmpId update EmployeeDetail set Name = @Name, MobilePhone = @MobilePhone, JobClass = @JobClass, Department = @Department WHERE EmployeeId = @EmpId";

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var isSuccess = connection.Execute(sql, new { EmpId = emp.ID});
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        public void Insert(EFReimburseEntities.Reimburse emp, string createdBy)
        {
            Guid id = Guid.NewGuid();
            string sqlEmp = "insert into Employee (Id, Nik, Password, Role, IsDeleted, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) VALUES (@Id, @Nik, @Password, @Role, 0, GETDATE(), @CreatedBy, GETDATE(), @CreatedBy )";
            string sqlEmpDet = "insert into EmployeeDetail (EmployeeId, Name, MobilePhone, JobClass, Department, JoinDate, IsDeleted, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) VALUES (@Id, @Name, @MobilePhone, @Jobclass,  @Department, @JoinDate, 0, GETDATE(), @CreatedBy, GETDATE(), @CreatedBy )";
            string sql = sqlEmp + " " + sqlEmpDet;

            using (var connection = _context.CreateMasterConnection())
            {
                try
                {
                    var isSuccess = connection.Execute(sql, new { Id = id, CreatedBy = createdBy});
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

    }
}


﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace ReimburseApi.EFReimburseEntities
{
    public partial class ReimburseBudget
    {
        public Guid ID { get; set; }
        public Guid ReimburseTypeId { get; set; }
        public string JobClass { get; set; }
        public decimal Budget { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ReimburseType ReimburseType { get; set; }
    }
}

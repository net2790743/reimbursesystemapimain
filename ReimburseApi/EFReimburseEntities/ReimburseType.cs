﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace ReimburseApi.EFReimburseEntities
{
    public partial class ReimburseType
    {
        public ReimburseType()
        {
            ReimburseBudget = new HashSet<ReimburseBudget>();
            ReimburseDetail = new HashSet<ReimburseDetail>();
        }

        public Guid ID { get; set; }
        public string Name { get; set; }
        public int ResetDuration { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<ReimburseBudget> ReimburseBudget { get; set; }
        public virtual ICollection<ReimburseDetail> ReimburseDetail { get; set; }
    }
}

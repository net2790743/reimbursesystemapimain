﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace ReimburseApi.EFReimburseEntities
{
    public partial class ReimburseDetail
    {
        public Guid ID { get; set; }
        public Guid ReimburseId { get; set; }
        public decimal TotalClaim { get; set; }
        public Guid ReimburseTypeId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual Reimburse Reimburse { get; set; }
        public virtual ReimburseType ReimburseType { get; set; }
    }
}

﻿using Microsoft.Extensions.Options;
using System;
using System.Net;
using ReimburseApi.Helpers;
using ReimburseApi.Models;
using ReimburseApi.Repositories;
using ReimburseApi.EFReimburseEntities;
using System.Collections.Generic;

namespace ReimburseApi.Services
{
    public interface IReimburseService
    {
        public WebApiResponseModel Get(Guid id, string role);
        public WebApiResponseModel GetDetail(Guid id);
    }

    public class ReimburseService : IReimburseService
    {
        private readonly IOptions<AppSettings> _config;
        private readonly IReimburseRepository _reimburseRepository;
        private readonly Logging _logging;
        public ReimburseService(IOptions<AppSettings> config, Logging logging, IReimburseRepository reimburseRepository)
        {
            _config = config;
            _reimburseRepository = reimburseRepository;
            _logging = logging;
        }

        public WebApiResponseModel Get(Guid id, string role = "")
        {
            var response = new WebApiResponseModel();
            var reimburse = new List<Reimburse>();

            if (role == "Administrator")
            {
                reimburse = _reimburseRepository.GetById(id);
            }
            else
            {
                reimburse = _reimburseRepository.GetAll();
            }

            response.data = reimburse;
            response.statusCode = ((int)HttpStatusCode.OK).ToString();
            response.message = "Success";

            return response;
        }

        public WebApiResponseModel GetDetail(Guid id)
        {
            var response = new WebApiResponseModel();
            var reimburse = new Reimburse();

            reimburse = _reimburseRepository.GetByReimburseId(id);
            if (reimburse != null)
            {
                var tempDetail = _reimburseRepository.GetByReimburseDetail(reimburse.ID);
                if (tempDetail != null)
                {
                    var tempType = _reimburseRepository.GetReimburseType(tempDetail.ReimburseTypeId);
                    if (tempType != null)
                    {
                        tempDetail.ReimburseType = tempType;
                    }
                    reimburse.ReimburseDetail.Add(tempDetail);
                }
            }

            response.data = reimburse;
            response.statusCode = ((int)HttpStatusCode.OK).ToString();
            response.message = "Success";

            return response;
        }

        public WebApiResponseModel UpdateUser(EFReimburseEntities.Reimburse reim)
        {
            var response = new WebApiResponseModel();

            _reimburseRepository.Update(reim);
            response.data = new EFReimburseEntities.Reimburse();
            response.statusCode = ((int)HttpStatusCode.OK).ToString();
            response.message = "Success";

            return response;
        }

        public WebApiResponseModel InsertUser(EFReimburseEntities.Reimburse reim, string createdBy)
        {
            var response = new WebApiResponseModel();


            _reimburseRepository.Insert(reim, createdBy);
            response.data = new EFReimburseEntities.Reimburse();
            response.statusCode = ((int)HttpStatusCode.OK).ToString();
            response.message = "Success";

            return response;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ReimburseApi.Services;
using ReimburseApi.Models;

namespace ReimburseApi.Controllers
{
    [ApiVersion("1")]
    [Route("[controller]")]
    [ApiController]
    public class ReimburseController : Controller
    {
        private readonly IReimburseService _reimburseService;

        public ReimburseController(IReimburseService reimburseService)
        {
            _reimburseService = reimburseService;
        }


        [Route("Get")]
        [HttpGet]
        [MapToApiVersion("1")]
        public IActionResult Get(System.Guid id, string role)
        {
            var response = _reimburseService.Get(id, role);
            return Ok(response);
        }

        [Route("GetDetail")]
        [HttpGet]
        [MapToApiVersion("1")]
        public IActionResult GetDetail(System.Guid id)
        {
            var response = _reimburseService.GetDetail(id);
            return Ok(response);
        }

        [Route("Update")]
        [HttpPost]
        [MapToApiVersion("1")]
        public IActionResult Update(EFReimburseEntities.Reimburse reimburse)
        {
            //var response = _reimburseService.UpdateUser(reimburse);
            var response = new WebApiResponseModel();
            return Ok(response);
        }

        [Route("Insert")]
        [HttpPost]
        [MapToApiVersion("1")]
        public IActionResult Insert(EFReimburseEntities.Reimburse reimburse, string createdBy)
        {
            //var response = _reimburseService.InsertUser(reimburse, createdBy);
            var response = new WebApiResponseModel();
            return Ok(response);
        }

    }
}
